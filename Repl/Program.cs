﻿using System;

using Lisp;
using Lisp.Model;

namespace Repl
{
    class Program
    {
        static void Main(string[] args)
        {
            var lexer = new Lexer();
            var parser = new Parser();
            var evaluator = new Evaluator();

            while (true)
            {
                Console.Write("> ");

                string line = Console.ReadLine();
                if (line == null)
                {
                    break;
                }

                try
                {
                    string[] lexed = lexer.Lex(line);
                    Expression expr = parser.Parse(lexed);
                    Expression result = evaluator.Evaluate(expr);

                    if (result is Printable p)
                    {
                        Console.WriteLine(p.AsString);
                    }
                    else
                    {
                        Console.WriteLine("[unprintable]");
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.ToString());
                }
            }
        }
    }
}
