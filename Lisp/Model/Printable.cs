﻿namespace Lisp.Model
{
    public interface Printable
    {
        string AsString { get; }
    }
}
