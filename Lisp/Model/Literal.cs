﻿namespace Lisp.Model
{
    public class Literal<T> : Atom, Printable
    {
        public T Value;

        public Literal(T value)
        {
            Value = value;
        }

        public string AsString => $"{Value}";
    }
}
