﻿namespace Lisp.Model
{
    public class Pair : Expression, Printable
    {
        public Expression Head;
        public Expression Tail;

        public Pair(Expression head, Expression tail)
        {
            Head = head;
            Tail = tail;
        }

        public string AsString => $"({(Head as Printable).AsString} {(Tail as Printable).AsString})";
    }
}
