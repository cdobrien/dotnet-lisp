﻿using System;

namespace Lisp.Model
{
    public class Function : Atom
    {
        public Func<Expression, Expression> Apply { get; }

        public Function(Func<Expression, Expression> func)
        {
            Apply = func;
        }
    }
}
