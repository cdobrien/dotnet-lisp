﻿using System;

namespace Lisp.Model
{
    public class NamedFunction : Function, Printable
    {
        public string Name { get; }

        public NamedFunction(string name, Func<Expression, Expression> func) : base(func)
        {
            Name = name;
        }

        public string AsString => $"[func {Name}]";
    }
}
