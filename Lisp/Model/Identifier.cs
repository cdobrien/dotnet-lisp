﻿namespace Lisp.Model
{
    public class Identifier : Atom, Printable
    {
        public string Name;

        public Identifier(string name)
        {
            Name = name;
        }

        public string AsString => Name;
    }
}
