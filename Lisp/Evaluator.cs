﻿using System.Collections.Generic;

using Lisp.Model;

namespace Lisp
{
    public class Evaluator
    {
        private readonly Dictionary<string, Expression> symbols;

        public Evaluator()
        {
            symbols = new Dictionary<string, Expression>()
            {
                { BuiltIns.Add.Name, BuiltIns.Add },
                { BuiltIns.Subtract.Name, BuiltIns.Subtract },
                { BuiltIns.Multiply.Name, BuiltIns.Multiply },
                { BuiltIns.Divide.Name, BuiltIns.Divide }
            };
        }

        public Expression Evaluate(Expression expr)
        {
            if (expr is Atom)
            {
                if (expr is Identifier id)
                {
                    return Lookup(id);
                }

                return expr;
            }

            var pair = expr as Pair;
            Expression head = Evaluate(pair.Head);
            Expression tail = Evaluate(pair.Tail);

            if (head is Function)
            {
                var func = head as Function;
                return func.Apply(tail);
            }

            return new Pair(head, tail);
        }

        private Expression Lookup(Identifier id)
        {
            return symbols[id.Name];
        }
    }
}
