﻿using System;
using System.Collections.Generic;

using Lisp.Model;

namespace Lisp
{
    public class Parser
    {
        private enum State {
            Initial,
            PairStart,
            GotHead,
            GotTail,
            InList
        }

        public class InvalidSyntaxException : Exception
        {
        }

        private State state = State.Initial;
        private Stack<Expression> exprStack = new Stack<Expression>();
        private Stack<State> stateStack = new Stack<State>();

        public Expression Parse(string[] input)
        {
            foreach (string s in input)
            {
                try
                {
                Process(s);
                }
                catch (Exception)
                {
                    throw new InvalidSyntaxException();
                }
            }

            if (exprStack.Count != 1)
            {
                throw new InvalidSyntaxException();
            }

            return exprStack.Pop();
        }

        private void Process(string s)
        {
            switch (s)
            {
            case "(":
                HandleOpen();
                break;

            case ")":
                HandleClose();
                break;

            default:
                ParseAtom(s);
                break;
            }
        }

        private void HandleOpen()
        {
            stateStack.Push(state);
            state = State.PairStart;
        }

        private void HandleClose()
        {
            if (state == State.GotTail)
            {
                Pair p = MakePair();
                exprStack.Push(p);
            }
            
            State oldState = stateStack.Pop();
            switch (oldState)
            {
            case State.PairStart:
                state = State.GotHead;
                break;
            case State.GotHead:
                state = State.GotTail;
                break;
            case State.GotTail:
                state = State.InList;
                break;
            case State.InList:
                state = oldState;
                break;
            }
        }

        private Pair MakePair()
        {
            Expression tail = exprStack.Pop();
            Expression head = exprStack.Pop();
            return new Pair(head, tail);
        }

        private void ParseAtom(string s)
        {
            Atom atom = ExtractAtom(s);
            Pair p;

            switch (state)
            {
            case State.Initial:
                exprStack.Push(atom);
                break;
            case State.PairStart:
                exprStack.Push(atom);
                state = State.GotHead;
                break;
            case State.GotHead:
                exprStack.Push(atom);
                state = State.GotTail;
                break;
            case State.GotTail:
                p = MakePair();
                p.Tail = new Pair(p.Tail, atom);
                exprStack.Push(p);
                state = State.InList;
                break;
            case State.InList:
                p = exprStack.Pop() as Pair;
                Pair q = Append(p, atom);
                exprStack.Push(q);
                state = State.InList;
                break;
            }
        }

        private Atom ExtractAtom(string s)
        {
            if (int.TryParse(s, out int value))
            {
                return new Literal<int>(value);
            }
            else
            {
                return new Identifier(s);
            }
        }

        private Pair Append(Pair pair, Expression expr)
        {
            if (pair.Tail is Pair p)
            {
                var tail = Append(p, expr);
                return new Pair(pair.Head, tail);
            }
            else
            {
                var tail = new Pair(pair.Tail, expr);
                return new Pair(pair.Head, tail);
            }
        }
    }
}
