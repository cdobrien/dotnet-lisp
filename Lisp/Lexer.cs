﻿using System.Collections.Generic;

namespace Lisp
{
    public class Lexer
    {
        private List<string> lexed;
        private string current;

        public string[] Lex(string input)
        {
            lexed = new List<string>();
            current = "";

            foreach (char c in input)
            {
                switch (c)
                {
                case ' ':
                    Break();
                    break;
                case '(':
                case ')':
                    Break();
                    lexed.Add(c.ToString());
                    break;
                default:
                    current += c;
                    break;
                }
            }

            Break();
            return lexed.ToArray();
        }

        private void Break()
        {
            if (current != "")
            {
                lexed.Add(current);
                current = "";
            }
        }
    }
}
