﻿using Lisp.Model;

namespace Lisp
{
    internal static class BuiltIns
    {
        public static NamedFunction Add => new NamedFunction("add", expr =>
        {
            var pair = expr as Pair;
            var x = pair.Head as Literal<int>;
            var y = pair.Tail as Literal<int>;

            int result = x.Value + y.Value;
            return new Literal<int>(result);
        });

        public static NamedFunction Subtract => new NamedFunction("sub", expr =>
        {
            var pair = expr as Pair;
            var x = pair.Head as Literal<int>;
            var y = pair.Tail as Literal<int>;

            int result = x.Value - y.Value;
            return new Literal<int>(result);
        });

        public static NamedFunction Multiply => new NamedFunction("mul", expr =>
        {
            var pair = expr as Pair;
            var x = pair.Head as Literal<int>;
            var y = pair.Tail as Literal<int>;

            int result = x.Value * y.Value;
            return new Literal<int>(result);
        });

        public static NamedFunction Divide => new NamedFunction("div", expr =>
        {
            var pair = expr as Pair;
            var x = pair.Head as Literal<int>;
            var y = pair.Tail as Literal<int>;

            int result = x.Value / y.Value;
            return new Literal<int>(result);
        });
    }
}
