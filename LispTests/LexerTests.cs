using Lisp;

using Xunit;

namespace LispTests
{
    public class LexerTests
    {
        [Theory]
        [InlineData("foo bar baz", new string[] { "foo", "bar", "baz" })]
        [InlineData("42 quux", new string[] { "42", "quux" })]
        public void Words_are_seperated(string input, string[] expected)
        {
            var l = new Lexer();
            var lexed = l.Lex(input);
            Assert.Equal(expected, lexed);
        }

        [Theory]
        [InlineData("(foo bar) baz", new string[] { "(", "foo", "bar", ")", "baz" })]
        [InlineData("((42 quux))", new string[] { "(", "(", "42", "quux", ")", ")" })]
        public void Parens_are_seperated(string input, string[] expected)
        {
            var l = new Lexer();
            var lexed = l.Lex(input);
            Assert.Equal(expected, lexed);
        }
    }
}
