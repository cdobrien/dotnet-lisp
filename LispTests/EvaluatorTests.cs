using Lisp;
using Lisp.Model;

using Xunit;

namespace LispTests
{
    public class EvaluatorTests
    {
        [Theory]
        [InlineData("add", 2, 5, 7)]
        [InlineData("add", 298, 717, 1015)]
        [InlineData("sub", 12, 4, 8)]
        [InlineData("sub", 937, 3289, -2352)]
        [InlineData("mul", 6, 3, 18)]
        [InlineData("mul", 687, 882, 605934)]
        [InlineData("div", 72, 9, 8)]
        [InlineData("div", 8552, 673, 12)]
        public void Binary_integer_arithmetic_works(string op, int x, int y, int expected)
        {
            var id = new Identifier(op);
            var a = new Literal<int>(x);
            var b = new Literal<int>(y);
            var expr = new Pair(id, new Pair(a, b));

            var e = new Evaluator();
            var result = e.Evaluate(expr) as Literal<int>;

            Assert.NotNull(result);
            Assert.Equal(expected, result.Value);
        }
    }
}
