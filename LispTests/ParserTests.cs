using Lisp;
using Lisp.Model;

using Xunit;

namespace LispTests
{
    public class ParserTests
    {
        [Theory]
        [InlineData(new string[] { "42" }, 42)]
        [InlineData(new string[] { "69" }, 69)]
        public void Can_parse_integer_literal(string[] input, int value)
        {
            var p = new Parser();
            Expression expr = p.Parse(input);
            var lit = expr as Literal<int>;
            Assert.NotNull(lit);
            Assert.Equal(value, lit.Value);
        }

        [Theory]
        [InlineData(new string[] { "foo" }, "foo")]
        [InlineData(new string[] { "bar" }, "bar")]
        public void Can_parse_identifier(string[] input, string name)
        {
            var p = new Parser();
            Expression expr = p.Parse(input);
            var ident = expr as Identifier;
            Assert.NotNull(ident);
            Assert.Equal(name, ident.Name);
        }

        [Theory]
        [InlineData(new string[] { "(", "42", "69", ")" }, 42, 69)]
        [InlineData(new string[] { "(", "69", "42", ")" }, 69, 42)]
        public void Can_parse_pair_of_integers(string[] input, int value0, int value1)
        {
            var p = new Parser();
            Expression expr = p.Parse(input);
            var pair = expr as Pair;
            Assert.NotNull(pair);

            Literal<int> lit;

            lit = pair.Head as Literal<int>;
            Assert.NotNull(lit);
            Assert.Equal(value0, lit.Value);

            lit = pair.Tail as Literal<int>;
            Assert.NotNull(lit);
            Assert.Equal(value1, lit.Value);
        }

        [Theory]
        [InlineData(new string[] { "(", "foo", "42", ")" }, "foo", 42)]
        [InlineData(new string[] { "(", "bar", "69", ")" }, "bar", 69)]
        public void Can_parse_pair_of_identifier_and_integer(string[] input, string name, int value)
        {
            var p = new Parser();
            Expression expr = p.Parse(input);
            var pair = expr as Pair;
            Assert.NotNull(pair);

            var ident = pair.Head as Identifier;
            Assert.NotNull(ident);
            Assert.Equal(name, ident.Name);

            var lit = pair.Tail as Literal<int>;
            Assert.NotNull(lit);
            Assert.Equal(value, lit.Value);
        }

        [Theory]
        [InlineData(new string[] { "(", "(", "foo", "42", ")", "bar", ")" }, "foo", 42, "bar")]
        [InlineData(new string[] { "(", "(", "bar", "69", ")", "foo", ")" }, "bar", 69, "foo")]
        public void Can_parse_pair_of_pair_of_identifier_and_integer_literal_and_identifier(string[] input, string name0, int value, string name1)
        {
            var p = new Parser();
            Expression expr = p.Parse(input);
            var pair0 = expr as Pair;
            Assert.NotNull(pair0);

            var pair1 = pair0.Head as Pair;
            Assert.NotNull(pair1);

            var ident0 = pair1.Head as Identifier;
            Assert.NotNull(ident0);
            Assert.Equal(name0, ident0.Name);

            var lit = pair1.Tail as Literal<int>;
            Assert.NotNull(lit);
            Assert.Equal(value, lit.Value);

            var ident1 = pair0.Tail as Identifier;
            Assert.NotNull(ident1);
            Assert.Equal(name1, ident1.Name);
        }

        [Fact]
        public void Can_parse_list()
        {
            string[] input = new string[] { "(", "foo", "bar", "baz", "42", ")" };
            var p = new Parser();
            Expression expr = p.Parse(input);

            var pair0 = expr as Pair;
            Assert.NotNull(pair0);

            var id0 = pair0.Head as Identifier;
            Assert.NotNull(id0);
            Assert.Equal("foo", id0.Name);

            var pair1 = pair0.Tail as Pair;
            Assert.NotNull(pair1);

            var id1 = pair1.Head as Identifier;
            Assert.NotNull(id1);
            Assert.Equal("bar", id1.Name);

            var pair2 = pair1.Tail as Pair;
            Assert.NotNull(pair2);

            var id2 = pair2.Head as Identifier;
            Assert.NotNull(id2);
            Assert.Equal("baz", id2.Name);

            var lit = pair2.Tail as Literal<int>;
            Assert.NotNull(lit);
            Assert.Equal(42, lit.Value);
        }

        [Fact]
        public void Can_parse_list_containing_pair()
        {
            string[] input = new string[] { "(", "foo", "(", "bar", "baz", ")", "42", ")" };
            var p = new Parser();
            Expression expr = p.Parse(input);

            var pair0 = expr as Pair;
            Assert.NotNull(pair0);

            var id0 = pair0.Head as Identifier;
            Assert.NotNull(id0);
            Assert.Equal("foo", id0.Name);

            var pair1 = pair0.Tail as Pair;
            Assert.NotNull(pair1);

            var pair2 = pair1.Head as Pair;
            Assert.NotNull(pair2);

            var id1 = pair2.Head as Identifier;
            Assert.NotNull(id1);
            Assert.Equal("bar", id1.Name);

            var id2 = pair2.Tail as Identifier;
            Assert.NotNull(id2);
            Assert.Equal("baz", id2.Name);

            var lit = pair1.Tail as Literal<int>;
            Assert.NotNull(lit);
            Assert.Equal(42, lit.Value);
        }

        [Theory]
        [InlineData("foo", ")")]
        [InlineData("42", "foo")]
        [InlineData("(", "foo", "(", "bar", "42", ")")]
        public void InvalidSyntaxException_is_thrown_on_invalid_syntax(params string[] input)
        {
            var p = new Parser();
            Assert.Throws<Parser.InvalidSyntaxException>(() => p.Parse(input));
        }
    }
}
